/**
 * Ejercicio 3: Búsqueda de ficheros csv
 */

const fs = require('fs').promises;

contenido1 = "";
contenido2 = "";
contenido3 = "";

// Función para leer un fichero 
async function leerFichero(nombreFichero) {
    const data = await fs.readFile(nombreFichero, 'utf-8');
    return data;
}

// Función para agrupar un array en otro
function agruparArray(array, arrayFinal) {
    for (i = 0; i < array.length; i++) {
        arrayFinal.push(array[i]);
    }
}

// Lee los 3 ficheros uno detrás de otro
leerFichero('mock_data1.csv').then(data => {
    contenido1 = data;
    leerFichero('mock_data2.csv').then(data => {
        contenido2 = data;
        leerFichero('mock_data3.csv').then(data => {
            contenido3 = data;

            // Ficheros leídos. Ahora se agrupan en un mismo array
            array1 = contenido1.split("\n");
            array2 = contenido2.split("\n");
            array3 = contenido3.split("\n");
            arrayFinal = [];
            agruparArray(array1, arrayFinal);
            agruparArray(array2, arrayFinal);
            agruparArray(array3, arrayFinal);            
            
            // Busca la IP de la persona con nombre y apellidos
            nombre = "Cari";
            apellido = "Wederell";
            for (i = 0; i < arrayFinal.length; i++) {
                linea = arrayFinal[i].split(',');
                if ((linea[1] === nombre) && (linea[2] === apellido)) {
                    console.log("IP =", linea[5]);
                    break;
                }
            }
        })
    })
})
