/**
 * Ejercicio 2: Dado electrónico
 */

function lanzarDado() {
    return Math.floor(Math.random() * 6) + 1;
}

totalScore = 0;
while (totalScore < 50) {
    tirada = lanzarDado();
    totalScore += tirada;
    console.log("Lanzamiento de dado. Resultado: ", tirada);
    console.log("Puntuación total: ", totalScore);
}
console.log("-".repeat(40));
console.log("Fin de la partida");
console.log("Puntuación final: ", totalScore);