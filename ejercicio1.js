/**
 * Ejercicio 1: Calculadora
 */

function calculadora(numero1, numero2, operacion) {
    resultado = 0;
    switch (operacion) {
        case "+":
            resultado = numero1 + numero2;
            return resultado;
        case "-":
            resultado = numero1 - numero2;
            return resultado;
        case "*":
            resultado = numero1 * numero2;
            return resultado;
        case "/":
            resultado = numero1 / numero2;
            return resultado;
        case "**":
            resultado = numero1**numero2;
            return resultado;
        default:
            break;
    }

    return resultado;
}

numero1 = 2;
numero2 = 3;
// Operacion. suma (+), resta (-), multiplicación (*), división (/), potencia (**)
operacion = "-";
resultado = calculadora(numero1, numero2, operacion);
console.log('El resultado de la operación es ', resultado);